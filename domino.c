#include<stdio.h>
#include<stdlib.h>
#include<time.h>


int domino[28][2] =
             {{0,0},{0,1},{0,2},{0,3},{0,4},{0,5},{0,6},
              {1,1},{1,2},{1,3},{1,4},{1,5},{1,6},
              {2,2},{2,3},{2,4},{2,5},{2,6},
              {3,3},{3,4},{3,5},{3,6},
              {4,4},{4,5},{4,6},
              {5,5},{5,6},
              {6,6}};

void mostra_mao(int v[]);// mostra a mao do jogador
void sortear_mao(int v1[], int v2[]);// sorteia a mao do jogador
int pertence(int c, int v1[], int v2[]);// ve se uma peca ja foi sorteada
void imprimir_mesa(int a[]);//imprimi a mesa atual.
/* Heitor, a mesa, vai ser um vetor, mesa[2], que vai mostrar só as cabecas da mesa.Ai quando tu colocar uma peca, por exemplo, na mesa tem 0 e 2, ai o vetor mesa vai ser v[0] = 0, v[1] = 2. Se o jogador coloca um duque e quadra, ai tu vai dizer q o v[1] = 4, a mesa passara a ser 0 e 4.*/




int computador(int mao[], int mesa[], int j)
{
    int x, p, i,c=0;
    mostra_mao(mao);
    if(j==1)
    {
        p = (rand()%6) +1;
        x = mao[p-1];
    }
    else
    {   do
        {
            p = (rand()%6)+1;
            x = mao[p-1];
            c++;
            if(c==7)
                return x;
         }while(mao[p-1] ==30 || !((domino[x][0] == mesa[0]) || (domino[x][0] == mesa[1]) || (domino[x][1] == mesa[0]) || (domino[x][1] == mesa[1])));
    }
    for( i=p; i<6; i++)
        mao[i-1] = mao[i];
    mao[5] = 30;
    return x;
}


void imprimir_mesa(int mesa[])
{
    printf("\n%d:%d\n", mesa[0], mesa[1]);
}


void  mostra_mao(int mao[])
{
    int n;
    for(n=0;n<6; n++)
    {
        if(mao[n] == 30)
            break;
       printf(" %d", mao[n]);
    }
}


void sortear_mao (int v1[7], int v2[7])
{
     int a, n, c=0;
     while(c <2)
     {
         for(a=0; a<6; a++)
         {
             do
             {
                 n = rand()%28;// n é o número que representa a peça, por exemplo carroça de branco é 0, braco e pio é 1, carroça de doze é 27.
             }while(pertence(n, v1, v2));
             if(c ==0)
                  v1[a] =n;
             else
                 v2[a] = n;

    	  }
      	  c++;
      }
}


int pertence(int x, int v1[6], int v2[6] )//Confere se a peça ja foi sorteada
{
    int i;
    for(i = 0; i<6; i++)
    {
       if( x== v1[i] || x== v2[i])
           return 1;
    }
    return 0;
}

