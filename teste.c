#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int batida = 0;//variavel para guardar se o usuario bateu ou nao.
int domino[28][2]= //matriz que guarda as pecas do jogo, as pecas estao numeradas de 0 a 27, assim cada peca é um vetor.Ex: 0:0, é o vetor domino[0][0],domino[0][1]
			{{0,0},{0,1},{0,2},{0,3},{0,4},{0,5},{0,6},
			 {1,1},{1,2},{1,3},{1,4},{1,5},{1,6},
			 {2,2},{2,3},{2,4},{2,5},{2,6},
			 {3,3},{3,4},{3,5},{3,6},
			 {4,4},{4,5},{4,6},
  			 {5,5},{5,6},
			 {6,6}};

int pertence2(int x, int v1[], int v2[]);
void mostra_mao(int mao[], int numeor_jogadores);
void sortear_mao(int mao1[], int mao2[], int mao3[], int mao4[], int numero_jogadores);
int pertence(int x, int mao1[], int mao2[], int mao3[],int mao4[]);
int humano(int v[], int k[], int j, int numero_jogadores);
int computador(int v[], int k[], int j, int numero_jogadores);
void jogo(int v1[], int v2[], int v3[], int v4[], int mesa[], int numero_jogadores, char j1, char j2, char j3, char j4);
void imprimir_mesa( int a[]);


int main(void)
{
	int mao1[6], mao2[6],mao3[6], mao4[6], mao12[12], mao22[12], mao32[12], mao42[12], mesa[2], k;//temos os vetores que guardam a mao do jogador, sendo os de 6, para o jogo de 4 pessoas, e o de 12 para o jogo de 2 pessoas.
	char j1,j2,j3,j4;//guarda o que cada jogador é, humano ou computador.
	srand(time(NULL));
	printf("Quantos jogadores, 2 ou 4?\n");
	scanf("%d", &k);//armazena se o jogo é um jogo se 4 ou de 2 pessoas.

	if(k==2)
	{
			sortear_mao(mao12, mao22, mao32, mao42, k);//sorteia a mao dos jogadores
			 printf("Digite 'c' se o jogador 1 for computador, e 'h' se for humano\n");//pergunta o que cada jogador é
			getchar();
			scanf("%c", &j1);
			 printf("Digite 'c' se o jogador 1 for computador, e 'h' se for humano\n");
			getchar();
			scanf("%c", &j2);
			printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");//limpa a tela
			jogo(mao12, mao22, mao32, mao42, mesa, k, j1, j2, j3, j4);//comeca a partida, mesa é um vetor que guarda as cabecas da mesa.
	}
	if(k==4)//analogo ao de cima, porém para um jogo de 4 jogadores.
	{
		sortear_mao(mao1, mao2, mao3, mao4, k);
		printf("Digite 'c' se o jogador 1 for computador, e 'h' se for humano\n");
		getchar();
		scanf("%c", &j1);
		printf("Digite 'c' se o jogador 2 for computador, e 'h' se for humano\n");
		getchar();
		scanf("%c", &j2);
		printf("Digite 'c' se o jogador 3 for computador, e 'h' se for humano\n");
		getchar();
		scanf("%c", &j3);	
		printf("Digite 'c' se o jogador 4 for computador, e 'h' se for humano\n");
		getchar();
		scanf("%c", &j4);
		printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		jogo(mao1, mao2, mao3, mao4, mesa, k, j1, j2, j3, j4);
	}

	return 0;
}


void jogo(int mao1[], int mao2[], int mao3[], int mao4[], int mesa[], int numero_jogadores, char j1, char j2, char j3, char j4)
{
		int x, rodada = 1,a, tocadas=0;//x guarda o numero da peca escolhida, 'a' guarda se é a primeira jogada do jogo, rodada guarda a rodada do jogo, e tocadas irá guardar se todo mundo tocou.
		a=1;		
		if(j1 == 'c')//se o jogador for um computador vai para a funcao computador
			x = computador(mao1, mesa, a,numero_jogadores);
		else 
			x = humano(mao1,mesa,a, numero_jogadores);
		mesa[0]= domino[x][0];//armazena mesa, veja que x é o numero da peca, de 0 a 27.
		mesa[1]= domino[x][1];
		printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		imprimir_mesa(mesa);//imprimi a mesa
		a++;
		while( !batida)//continua se ninguem bater ainda.
		{
			
			if(numero_jogadores ==2)//partida de 2 jogadores
			{
				if(tocadas==2)//se os dois jogadores tocaram seguidos, o jogo acaba.
				{
					printf("fechou o jogo\n");
					return;
				}
				if(rodada%2)//se for uma rodada par, é a vez do jogador 2.
				{
					if(j2 == 'c')//ve se o jogador 2 é um humano ou computador
						x = computador(mao2, mesa, a, numero_jogadores);
					else
						x = humano(mao2,mesa,a, numero_jogadores);
				}
				else //se a rodada for impar, é a vez do jogador 1.
				{
					if(j1 == 'c')
						x = computador(mao1, mesa, a, numero_jogadores);
					else
						x = humano(mao1, mesa, a, numero_jogadores);
				}
				rodada++;//adiciona 1 a rodada
				if(x==30)//quando um jogador toca, ele digita 30, assim se x é 30, aquele jogador tocou.
					tocadas++;
				else//se o jogador nao tocou, entao tocadas seguidas é zerado.
					tocadas =0;
			}
			else//jogo de 4 pessoas.
			{
				if(tocadas ==4)//analogo ao de 2 pessoas, porém é necessario 4 tocadas.
				{
					printf("fechou o jogo\n");
					return;
				}
				rodada++;
				if (rodada ==5)//se a rodada for a 5, é reiniciado o contador para 1.
					rodada =1;
				switch(rodada)
				{
					case 1 : if(j1 == 'c')
		                         x = computador(mao1, mesa, a, numero_jogadores);
        		             else
                		         x = humano(mao1, mesa, a, numero_jogadores);break;
					case 2 : if(j2 == 'c')
							     x = computador(mao2, mesa, a, numero_jogadores);
							else
							     x = humano(mao2, mesa, a, numero_jogadores);break;
					case 3 : if(j3 == 'c')
							     x = computador(mao3, mesa, a, numero_jogadores);
							else
							     x = humano(mao3, mesa, a, numero_jogadores);break;
					case 4: if(j4 == 'c')
							     x = computador(mao4, mesa, a, numero_jogadores);
							else
							     x = humano(mao4, mesa, a, numero_jogadores);break;
				}
				if(x==30)
					tocadas++;
				else
					tocadas =0;
			}
		//se a peca escolhida for igual a uma cabeca da mesa, o outro lado da peca substitui a cabeca.
		if(mesa[0] == domino[x][0])
		{	 mesa[0] = domino[x][1];
			a++;}
		else 
		{
			if(mesa[0] == domino[x][1])
			{	 mesa[0] = domino[x][0];
				a++;}
			else
			{
				 if(mesa[1] == domino[x][0])
				 {	mesa[1] = domino[x][1];
					a++;}
				else
				{
					if(mesa[1] == domino[x][1])
					{	 mesa[1] = domino[x][0];
						a++;}
				}
			}
		}
		printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		imprimir_mesa(mesa);
		}
		printf("Bati");
}


int computador(int mao[], int mesa[], int j, int numero_jogadores)//funcao de jogada do computador.
{
	int x, p, i,c=0;
	mostra_mao(mao, numero_jogadores);//mostra a mao do jogador.
	if(j==1)//se for a primeira rodada, o jogador pode jogar qualquer peca.
	{
		if(numero_jogadores ==2)//dependendo se for um jogo de 2 ou de 4 pessoas, sorteia entre o vetor de 6 ou de 12 pecas.
			p = (rand()%12) +1;//sorteia uma peca qualquer.
		else
			p = (rand()%6) +1;
		x = mao[p-1];
	}
	else
	{	do 
		{
			if(numero_jogadores ==2)
				p = (rand()%12)+1;//sorteia uma peca, entre 1 e 12.
			else
				p = (rand()%6) +1;//sorteia uma peca, entre 1 e 6.
			x = mao[p-1];//x é numero da peca, vai ser igual a peca correspondente ao numero sorteado no vetor mao do jogador.
			c++;//cotador de vezes que o sorteio de uma peca foi feito
			if(numero_jogadores ==2)
			{
				if(c==11)//para um jogo de 2 pessoas, se ja foi sorteado 11 vezes, quer dizer que o jogador não possui uma peca valida
					return x;// assim ele irá retornar x, que vale 30, se a peca nao  for valida, explicao disso mais a frente.	
			}
			else
			{
				if(c==5)//para um jogo de 4 pessoas, se foi sorteado 5 vezes.
					return x;
			}
			//a condicao a baixo, se o numero sortado, um lado da peca correspondente nao estiver na mesa, ou o numero correponder a 30 na mao do jogador,ó sorteio é feito novamente.
		 }while(mao[p-1] ==30 || !((domino[x][0] == mesa[0]) || (domino[x][0] == mesa[1]) || (domino[x][1] == mesa[0]) || (domino[x][1] == mesa[1])));
	}
	if(numero_jogadores ==2)//para um jogo de 2 pessoas
	{
		for( i=p; i<12; i++)//irá retirar a peca escolhida da mao do jogador
			mao[i-1] = mao[i];
		mao[11] = 30;// a ultima peca será 30, depois disso o valor 30 ficará sendo puxado para as pecas seguintes.
	}
	else//analogo ao de cima, para um jogode 4 pessoas
	{
		for(i=p;i<6;i++)
			mao[i-1] = mao[i];
		mao[5] = 30;
	}
	if(mao[0] ==30)//se o numeor primeira "peca" for 30, quer dizer que o jogador jogou sua ultima peca e bateu.
		batida =1;
	return x;
}


void imprimir_mesa(int mesa[])//imprimi a mesa
{
	printf("\n%d:%d\n", mesa[0], mesa[1]);//imprimi as cabecas da mesa.
}


int humano(int v[], int mesa[], int j, int numero_jogadores)//funcao do jogador humano
{
	int p, i, x,c=0;
	mostra_mao(v, numero_jogadores);//mostra mao do jogador
	if(j==1)//se for a primeira jogada, o jogador pode joga qualquer peca.
	{
		printf("\nDigite a ordem da peca que deseja jogar\n");//o jogador digita a ordem da peca de sua mao.
		scanf("%d", &p);
		x = v[p-1];//x marca o valor daquela peca.No vetor que guarda a mao do jogador, está guardado os numeros correspondente as pecas, de 0 a 27.
	}
	else
	{
		do
		{
			if(c>0)// nesse caso, o loop rodou mais de uma vez, assim o jogador escolheu uma peca invalida.
				printf("Olha o gato por lebre");
			printf("\nDigite a ordem da peca que deseja jogar, caso nao possua peca, digite 30\n");
			scanf("%d", &p);
			if( p ==30)//se o jogador nao possui nenhuma peca, ele deve digitar 30, e a funcao irá retornar 30. 
				return 30;
			x = v[p-1];
			c++;
		//a condicao a baixo, verifica se a peca escolhida pode ser jogada na mesa
		}while( !((domino[x][0] == mesa[0]) || (domino[x][0] == mesa[1]) || (domino[x][1] == mesa[0]) || (domino[x][1] == mesa[1])) );
	}
	if(numero_jogadores == 2)//para um jogo de 2 pessoas
	{
		for(i = p; i<12; i++)
			v[i-1] = v[i];//vai tirar a peca jogada do vetor mao
		v[11] = 30;//coloca 30 na ultima posicao, e esse 30 irá andando ao longo do vetor.
	}
	else
	{
		for(i=p; i<6;i++)
			v[i-1] = v[i];
		v[5] =30;
	}
	if(v[0] ==30)//quando a primeira peca for 30, o jogador não tem mais pecas, portanto ele bateu.
		batida = 1;
	return x;
}


void  mostra_mao(int mao[], int a)//funcao que mostra a mao do jogador.
{
	int n;
	int x;
	if(a ==2)//para um jogo de 2 pessoas
	{
		for(n=0; n<12; n++)//vai percorrendo o vetor mao, e imprimindo.
		{
			x = mao[n];
			if(mao[n] == 30)//quando encontrar 30 no vetor, quer dizer que ja foram imprimidas todas as pecas.
				break;
			printf(" %d:%d", domino[x][0], domino[x][1] );//imprimi a peca, x é o numero que marca a peca, de 0 a 27.
		}
	}
	if(a==4)//analogo, para um jogo de 4 pessoas.
	{	
		for(n=0; n<6; n++)
		{
			x= mao[n];
			if(mao[n] ==30)
				break;
			printf(" %d:%d", domino[x][0], domino[x][1] );
		}
	}
}


void sortear_mao (int v1[], int v2[], int v3[], int v4[], int numero_jogadores)//funcao para sortear a mao dos jogadores.
{

	int a, n, c=0;
	if (numero_jogadores == 2)//para um jogo de 2 pessoas, portanto 12 pecas para cada jogador
	{
		while(c <2)
		{
			for(a=0; a<12; a++)
			{
				do
				{
					n = rand()%28;// n é o número que representa a peça, por exemplo carroça de branco é 0, braco e pio é 1, carroça de doze é 27.
				}while(pertence2(n, v1, v2));//verifica se peca ja foi escolhida
				if(c ==0)
					 v1[a] =n;
				else 
					v2[a] = n;
			}
			c++;
		}
	}
	else//analogo para um jogo de 4 pessoas, portanto 6 pecas para cada jogador
	{
		while(c<4)
		{
			for(a=0;a<6;a++)
			{
				do
				{
					n = rand()%28;
				}while(pertence(n, v1, v2, v3, v4));
				switch(c)
				{
					case 0 : v1[a] = n; break;
					case 1 : v2[a] = n; break;
					case 2 : v3[a] = n; break;
	      			case 3 : v4[a] = n; break;
				}
			}
			c++;
		}
	}
}


int pertence2(int x, int v1[], int v2[])//verifica se a peca ja foi escolhida, num jogo de 2 pessoas
{
	int i;
	for(i = 0; i<12; i++)
    {
       if(x==v1[i] || x== v2[i] )
           return 1;
    }
    return 0;
}

	
int pertence(int x, int v1[], int v2[], int v3[], int v4[])//Confere se a peça ja foi sorteada, num jogo de 4 pessoas.
{
	int i;
	for(i = 0; i<6; i++)
	{
		if((x==v1[i] || x== v2[i]) || (x==v3[i] || x==v4[i]))
			return 1;
	}
	return 0;		
}

